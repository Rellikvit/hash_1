#include <iostream>
#include <string.h>
#include <vector>

using namespace std;

vector<long long int> newVector;

string stringgen(const int len) {
    static const char alphanum[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";
    std::string tmp_s;
    tmp_s.reserve(len);

    for (int i = 0; i < len; ++i) {
        tmp_s += alphanum[rand() % (sizeof(alphanum) - 1)];
    }

    return tmp_s;
}

long long int hashgen(string *s)
{
    int h = 0;
    for (int i = 0; i < strlen(s->c_str()); i++)
    {
        h = h * 101 + (unsigned)(s->c_str()[i]);
    }
    for(int i = 0; i < newVector.size();i++)
    {
        if (h == newVector[i])
        {
            h = i;
            break;
        }
    }
    return h;
}

int main()
{
    
    srand((unsigned)time(NULL));
    for (int i = 0; i < 100; i++)
    {
        string newString = stringgen(10);
        newVector.push_back(hashgen(&newString));
        cout << newString << " " << newVector[i] << " " << i << endl;
    }
    int colCount = 0;
    for (int j = 0; j < 100; j++)
    {
        for (int m = j+1; m < 99; m++)
        {
            if (newVector[j] == newVector[m])
            {
                colCount++;
            }
        }
    }
    cout << endl;
    if (colCount > 0)
    {
        cout << "Number of collisions: " << colCount << endl;
    }
    else
    {
        cout << "No Collisions!";
    }
}